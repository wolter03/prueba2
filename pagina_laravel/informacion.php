<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <title>Informacion</title>
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">    
</head>
<header>
	<p id="titulo">PHP-Laravel</p>
</header>
<body>
	<h1 id="h">La hora es: </h1>
	<h1 id="hora">
	<?php
        date_default_timezone_set("America/El_Salvador");
        echo date("h:i a");
	?>
	</h1>
	<p id="ksk">Los integrantes del grupo PHP-Laravel somos: </p>
	<div id="s1">
	<img id="fo1" src="imagenes/Anzora.jpg">
	<p id="in1">Nombre: Bryan Giovanni Anzora Perdomo</p>
	<p id="in1">Carnet: 20170442</p>
	<p id="in1">Sistemas Informáticos. Sección B</p>
	</div>
	<div id="s2">
	<img id="fo2" src="imagenes/Walter.jpg">
	<p id="in2">Nombre: Walter Alexis Carabantes Gutiérrez</p>
	<p id="in2">Carnet: 20170091</p>
	<p id="in2">Sistemas Informáticos. Sección B</p>
	</div>
	<div id="s3">
	<img id="fo3" src="imagenes/Chino.jpg">
	<p id="in3">Nombre: Bryan Enrique Fuentes Gómez</p>
	<p id="in3">Carnet: 20170103</p>
	<p id="in3">Sistemas Informáticos. Sección B</p>
	</div>
	<div id="s4">
	<img id="fo4"src="imagenes/Mario.jpg">
	<p id="in4">Nombre: Mario Emerson Hernández Fuentes</p>
	<p id="in4">Carnet: 20080019</p>
	<p id="in4">Sistemas Informáticos. Sección B</p>
	</div>
	<div id="s5">
	<img id="fo5" src="imagenes/Wil.jpg">
	<p id="in5">Nombre: Wilfredo Isaías Hernández Guirola</p>
	<p id="in5">Carnet: 20100279</p>
	<p id="in5">Sistemas Informáticos. Sección B</p>
	</div>	
</body>
</html>